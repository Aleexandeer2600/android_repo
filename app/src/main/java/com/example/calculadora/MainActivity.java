package com.example.calculadora;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
//import android.view.View;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    /*
     *
     * se declaran variables que se van a utilizar */
    TextView Resultado;
    double res;
    String operador, muestra, reserva;
    Button btnCero, btnCero2, btnUno, btnDos, btnTres, btnCuatro, btnCinco, btnSeis, btnSiete, btnOcho, btnNueve,
            btnSuma, btnResta, btnMult, btnDiv,
            btnIgual, btnClean, btnPun, btnBorra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
         * Inicializan las variable casteando el tipo de elemento */
        btnCero = (Button)findViewById(R.id.btn0);
        btnCero2 = (Button)findViewById(R.id.btn00);
        btnUno = (Button)findViewById(R.id.btn1);
        btnDos = (Button)findViewById(R.id.btn2);
        btnTres = (Button)findViewById(R.id.btn3);
        btnCuatro = (Button)findViewById(R.id.btn4);
        btnCinco = (Button)findViewById(R.id.btn5);
        btnSeis = (Button)findViewById(R.id.btn6);
        btnSiete = (Button)findViewById(R.id.btn7);
        btnOcho = (Button)findViewById(R.id.btn8);
        btnNueve = (Button)findViewById(R.id.btn9);
        btnSuma = (Button)findViewById(R.id.btn_suma);
        btnResta= (Button)findViewById(R.id.btn_resta);
        btnDiv = (Button)findViewById(R.id.btn_divi);
        btnMult = (Button)findViewById(R.id.btn_multi);
        btnClean = (Button)findViewById(R.id.btn_Clean);
        btnIgual = (Button)findViewById(R.id.btn_igual);
        Resultado = (TextView)findViewById(R.id.Etiqueta);
        btnPun = (Button)findViewById(R.id.btnpunto);
        btnBorra = (Button)findViewById(R.id.btn_Borra);


        ///////////////////ya estan ligados con la parte visual o .xml

        /*
         * Eventos */
        btnCero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "0";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnCero2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "00";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "1";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "2";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnTres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "3";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnCuatro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "4";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnCinco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "5";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnSeis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "6";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnSiete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "7";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnOcho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "8";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnNueve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "9";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reserva = Resultado.getText().toString();
                operador = "+";
                Resultado.setText("");
            }
        });

        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reserva = Resultado.getText().toString();
                operador = "-";
                Resultado.setText("");
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reserva = Resultado.getText().toString();
                operador = "/";
                Resultado.setText("");
            }
        });

        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reserva = Resultado.getText().toString();
                operador = "*";
                Resultado.setText("");
            }
        });

        btnPun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + ".";    //Se le asigna el valor del boton
                Resultado.setText(muestra); //muestra en la etiqueta
            }
        });

        btnClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muestra = ""; //se le asigna un espacio vacio
                Resultado.setText(muestra); //muestra en la etiqueta
                /*
                 * Se limpian ambas variables interactuan datos */
                reserva = "";
                operador = "";
            }
        });

        btnBorra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(muestra == null){
                    Toast.makeText(MainActivity.this, "NADA", Toast.LENGTH_SHORT).show();
                }else{
                    muestra = Resultado.getText().toString();
                    muestra = muestra.substring(0, muestra.length()-1); //retrocede un espacio eliminado el valor que exista detras
                }
                Resultado.setText(muestra); //muestra en la etiqueta

            }
        });
        ///////////////////////////////////////////////////////////////
        /*
         *
         * Logica operaciones*/

        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                muestra = Resultado.getText().toString(); //variable que contendra lo que el objeto de la etiqueta tenga
                muestra = muestra + "1";    //Se le asigna el valor del boton
                switch (operador){
                    case "+":
                        res = Double.parseDouble(reserva) + Double.parseDouble(Resultado.getText().toString());
                        Resultado.setText(String.valueOf(res));
                        break;

                    case "-":
                        res = Double.parseDouble(reserva) - Double.parseDouble(Resultado.getText().toString());
                        Resultado.setText(String.valueOf(res));
                        break;

                    case "/":
                        res = Double.parseDouble(reserva) / Double.parseDouble(Resultado.getText().toString());
                        Resultado.setText(String.valueOf(res));
                        break;

                    case "*":
                        res = Double.parseDouble(reserva) * Double.parseDouble(Resultado.getText().toString());
                        Resultado.setText(String.valueOf(res));
                        break;

                }

            }
        });


    }



}
